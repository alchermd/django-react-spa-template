import axios from "axios";
import { useState } from "react";
import './App.css';

function App() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [token, setToken] = useState("---");
  const [accountInfo, setAccountInfo] = useState("---");
  const apiRoot = process.env.REACT_APP_API_ROOT;

  if (!apiRoot) return <code>Unable to load REACT_APP_API_ROOT from .env.dev file</code>;

  const login = (email, password) => {
    const data = new FormData();
    data.set("email", email);
    data.set("password", password);
    axios.post(`${apiRoot}token/login/`, data)
      .then(({data}) => setToken(data.auth_token))
      .catch(e => alert(e.response.data.non_field_errors[0]));
  };

  const fetchInfo = () => {
    if (!token || token === "---") {
      alert("Fetch a token first.");
      return;
    }
    axios.get(`${apiRoot}users/me/`, {headers:{"Authorization": `Token ${token}`}})
      .then(({data}) => setAccountInfo(JSON.stringify(data)))
      .catch(e => {
        console.log(e);
        alert("Error logged in console.");
      });
  }

  return (
    <div className="App">
      <header className="App-header">
        <div>
          The API root is: <br/>
          <code>{apiRoot}</code> <br/>

          <hr/>
          <form onSubmit={(e) => {e.preventDefault(); login(email, password)}}>
            <input type="email"
              id="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
              placeholder="email"/>

            <br/>

            <input type="password"
              id="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
              placeholder="password"/>

            <br/>

            <input type="submit" value="Login"/>
          </form>

          Auth Token: <br/>
          <code>{token}</code>

          <hr/>
          <button onClick={fetchInfo}>Fetch Your Info</button> <br/>
          Account Info: <br/>
          <code>{accountInfo}</code>
        </div>
      </header>
    </div>
  );
}

export default App;
