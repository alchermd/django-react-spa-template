# Django API + React SPA Starter Template 

## Prerequisites

- Docker
- Docker Compose
- Make

## Usage

```console
$ cp .env.dev.example .env # edit as necessary
$ make rebuild
$ make migrate
$ make createsuperuser
```

The React app is now served on port 3000, while the Django API is served on port 8000.

Access the individual container logs:

```console
$ make apilogs
$ make uilogs
$ make dblogs
```

## API Docs

A Swagger UI documentation is available at http://localhost:8000/api/docs/
