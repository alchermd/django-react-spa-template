COMPOSE_CMD=docker-compose
DEFAULT_COMPOSE_FILE=docker-compose.yml
PY_MANAGE=exec api python manage.py

up:
	$(COMPOSE_CMD) -f $(DEFAULT_COMPOSE_FILE) up -d

rebuild:
	$(COMPOSE_CMD) -f $(DEFAULT_COMPOSE_FILE) up -d --build

api-logs:
	$(COMPOSE_CMD) -f $(DEFAULT_COMPOSE_FILE) logs -f api

ui-logs:
	$(COMPOSE_CMD) -f $(DEFAULT_COMPOSE_FILE) logs -f ui

db-logs:
	$(COMPOSE_CMD) -f $(DEFAULT_COMPOSE_FILE) logs -f db

down:
	$(COMPOSE_CMD) -f $(DEFAULT_COMPOSE_FILE) down

downv:
	$(COMPOSE_CMD) -f $(DEFAULT_COMPOSE_FILE) down -v

shell:
	$(COMPOSE_CMD) -f $(DEFAULT_COMPOSE_FILE) $(PY_MANAGE) shell

createsuperuser:
	$(COMPOSE_CMD) -f $(DEFAULT_COMPOSE_FILE) $(PY_MANAGE) createsuperuser

makemigrations:
	$(COMPOSE_CMD) -f $(DEFAULT_COMPOSE_FILE) $(PY_MANAGE) makemigrations --noinput

migrate:
	$(COMPOSE_CMD) -f $(DEFAULT_COMPOSE_FILE) $(PY_MANAGE) migrate --noinput

test:
	$(COMPOSE_CMD) -f $(DEFAULT_COMPOSE_FILE) $(PY_MANAGE) test --failfast --noinput

ptest:
	$(COMPOSE_CMD) -f $(DEFAULT_COMPOSE_FILE) $(PY_MANAGE) test --failfast --noinput --parallel

ui-sh:
	$(COMPOSE_CMD) -f $(DEFAULT_COMPOSE_FILE) exec ui sh 

api-sh:
	$(COMPOSE_CMD) -f $(DEFAULT_COMPOSE_FILE) exec api sh 
